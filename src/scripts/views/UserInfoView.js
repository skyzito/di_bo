import View from './View';
import ProfileImage from '../../images/profile.jpg';

class UserInfoView extends View {
	constructor(element) {
		super(element);
	}
	template(model) {
		return `
			<div class="col-3">
				<img src="${ProfileImage}" alt="profile">
			</div>
			<div class="col-5">
				<h1 class="username">${model.user.name}</h1>
                <p class="address">
                    <i class="icon ion-ios-location-outline"></i>
                    <span>${model.user.address}</span> 
                </p>
                <p class="phone">
                    <i class="icon ion-ios-telephone-outline"></i>
                    <span>${model.user.phone}</span> 
                </p>
                <p class="website">
                    <i class="icon ion-android-globe"></i>
                    <span>${model.user.website}</span> 
                </p>
			</div>
		`;
	}
}

export default UserInfoView;
