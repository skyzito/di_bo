import View from './View';

class UserFollowersView extends View {
	constructor(element) {
		super(element);
	}
	template(model) {
		return `
			<i class="icon ion-android-add-circle"></i>
            <span id="followersQuantity" class="count">${ model.user.followers }</span> 
            <span>Followers</span>
		`;
	}
}

export default UserFollowersView;
