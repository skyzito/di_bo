export default class View {
	constructor(element) {
		this._element = element;
	}
	template() {
		throw new Error('Needs implement Template Method');
	}
	update(model) {
		this._element.innerHTML = this.template(model);
	}
}
