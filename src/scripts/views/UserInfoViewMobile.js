import View from './View';

class UserInfoViewMobile extends View {
	constructor(element) {
		super(element);
	}
	template(model) {
		return `
			<div class="col-12">
                <p class="username">${model.user.name}</p>
                <p class="website">
                    <i class="icon ion-android-globe"></i>
                    <span>${model.user.website}</span> 
                </p>
                <p class="address">
                    <i class="icon ion-ios-location-outline"></i>
                    <span>${model.user.address}</span> 
                </p>
                <p class="phone">
                    <i class="icon ion-ios-telephone-outline"></i>
                    <span>${model.user.phone}</span> 
                </p>
			</div>
		`;
	}
}

export default UserInfoViewMobile;
