import View from './View';

class UserView extends View {
	constructor(element) {
		super(element);
	}
	template(model) {
		return `
			<fieldset>
				<form action="">
					<div class="form-group">
						<input type="text" value="${model.user.name}"  
							   name="name" 
							   id="name">
						<button type="button" 
								class="btn-edit-user">
								<i class="icon ion-edit"></i>
						</button>
						<div class="tooltip">
							<div class="group">      
								<input type="text" name="name" id="nameTooltip" value="${model.user.name}" required>
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Name</label>
								<div class="clearfix"></div>
								<button class="btn btn-edit" type="button">save</button>
							</div>
							
							<div class="arrow-wrapper">
								<div class="arrow-left"></div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="url" value="${model.user.website}" name="website" id="website">
						<button type="button" class="btn-edit-user">
								<i class="icon ion-edit"></i>
						</button>
						<i class="icon ion-android-globe icon-input"></i>

						<div class="tooltip">
							<div class="group">      
								<input type="text" name="websiteTooltip" id="websiteTooltip" value="${model.user.website}" required>
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Website</label>
								<div class="clearfix"></div>
								<button class="btn btn-edit" type="button">save</button>
							</div>
							
							<div class="arrow-wrapper">
								<div class="arrow-left"></div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="tel" value="${model.user.phone}" name="phone" id="phone">
						<button type="button" class="btn-edit-user">
								<i class="icon ion-edit"></i>
						</button>
						<i class="icon ion-ios-telephone-outline icon-input"></i>

						
						<div class="tooltip">
							<div class="group">      
								<input type="text" name="phoneTooltip" id="phoneTooltip" value="${model.user.phone}" required>
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Phone</label>
								<div class="clearfix"></div>
								<button class="btn btn-edit" type="button">save</button>
							</div>
							
							<div class="arrow-wrapper">
								<div class="arrow-left"></div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="text" value="${model.user.address}" name="address" id="address">
						<button type="button" class="btn-edit-user">
								<i class="icon ion-edit"></i>
						</button>
						<i class="icon ion-ios-home-outline icon-input"></i>

						<div class="tooltip">
							<div class="group">      
								<input type="text" name="addressTooltip" id="addressTooltip" value="${model.user.address}" required>
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Address</label>
								<div class="clearfix"></div>
								<button class="btn btn-edit" type="button">save</button>
							</div>
							
							<div class="arrow-wrapper">
								<div class="arrow-left"></div>
							</div>
						</div>
					</div>
				
				</form>
			</fieldset>
		`;
	}
}

export default UserView;
