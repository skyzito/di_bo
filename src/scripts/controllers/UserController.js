import ListUser from '../models/ListUser';
import UserView from '../views/UserView';
import UserInfoView from '../views/UserInfoView';
import UserInfoViewMobile from '../views/UserInfoViewMobile';
import UserFollowersView from '../views/UserFollowersView';

class UserController {
	constructor() {

		let $ = document.querySelector.bind(document);
		this._listUser = new ListUser();
		this._userView = new UserView($('#userView'));
		this._userInfoView = new UserInfoView($('#userInfo'));
		this._userInfoViewMobile = new UserInfoViewMobile($('#userInfoViewMobile'));
		this._userFollowersView = new UserFollowersView($('#userFollowers'));
		this._userView.update(this._listUser);
		this._userInfoView.update(this._listUser);
		this._userInfoViewMobile.update(this._listUser);
		this._userFollowersView.update(this._listUser);
		this.setUser = this.setUser.bind(this);
		this.addFollowerBtn = $('.add-follower');
		this.setFollower = this.setFollower.bind(this);
		this.countFollowers = 0;
		this.addFollowerBtn.addEventListener('click', this.setFollower, false);
		this.setPage = this.setPage.bind(this);
		this.setListenerEditBtns(this.setUser);
		this.setListenerMenuItems(this.setPage);
	}

	setUser() {
		this._listUser.edit(this.editUser());
		this._userInfoView.update(this._listUser);
		this._userView.update(this._listUser);
		this._userInfoViewMobile.update(this._listUser);
		this.setListenerEditBtns(this.setUser);
	}

	editUser() {
		let $ = document.querySelector.bind(document);
		let name = $('#nameTooltip').value;
		let website = $('#websiteTooltip').value;
		let phone = $('#phoneTooltip').value;
		let address = $('#addressTooltip').value;
		let user = {
			name : name,
			website: website,
			phone: phone,
			address: address,
		};

		return user;
	}

	setListenerEditBtns(setUser) {
		let editUserBtns  = [].slice.call(document.getElementsByClassName("btn-edit"));

		editUserBtns.forEach((element) => {
			element.addEventListener("click", setUser, false);
		});
	}

	setListenerMenuItems(setPage) {
		let menuItems  = [].slice.call(document.getElementsByClassName("item-menu"));

		menuItems.forEach((element) => {
			element.addEventListener("click", setPage, false);
		});
	}

	setPage(event) {
		let pageContainers  = [].slice.call(document.getElementsByClassName("page-container"));
		pageContainers.forEach((element) => {
			let elementClasses = element.className.split(" ");
			let lastClass = elementClasses.slice(-1);

			if (lastClass[0] === event.target.id){
				element.style.display = 'block';
			} else {
				element.style.display = 'none';
			}
		});
	}

	addFollower() {
		this.countFollowers++;
		let followers = this.countFollowers;

		return followers;
	}

	setFollower(){
		this._listUser.add(this.addFollower());
		this._userFollowersView.update(this._listUser);
	}
}

export default UserController;
