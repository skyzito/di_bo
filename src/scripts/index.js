if (module.hot) {
	module.hot.accept();
}

import 'babel-polyfill';
import '../styles/main.scss';


import UserController from './controllers/UserController';

class App {
	constructor() {
		let userController = new UserController();
		console.log(userController);
	}
}
const app = new App();
export default app;

