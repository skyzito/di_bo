class ListUser {
	constructor() {
		this._user = {
			name : "Jessica Parker",
			website: "www.diegobolina.com.br",
			phone: "(949) 325 - 68594",
			address: "Newport Beach, CA",
			followers: 0,
		};
	}
	edit(user) {
		return this._user = user;
	}
	add(followers){
		return this._user.followers = followers;
	}
	get user() {
		return this._user;
	}
	set user(user){
		this._user = user;
	}
}

export default ListUser;
